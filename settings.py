import json, os, logging

settings = 'settings.json'

'''
Initializes settings file and 
resets the contents to default state
'''
def init_settings():
    default_settings = {
        #SMS API related settings
        "username": "sandbox",
        "sms_api_key": "784abf520ce1e783e5f58caa77d43525c5909f50988a90192152fa065fee05aa",
        "short-code": "27460",
        #Wording related settings
        "successful": "Your Agent code is ",
        "duplicate": "You're already signed up as an agent with the code: "
    }
    try:
        f = open(settings, 'w+')
        json.dump(default_settings, f)
    except:
        logging.error(f'Settings: Error reseting env: {e}')
        f.close()
        return False
    finally:
        f.close()

def read_settings():
    if os.path.isfile(settings):
        try: 
            f = open(settings, 'r')
        except:
            c(f'Settings file not accessible {e}')
            f.close()
            return False
        rs = json.load(f)
        f.close()
        return rs
    else:
        rs = init_settings()
        if rs is False:
            logging.error(f'Cant create settings')
            return False
        try: 
            f = open(settings, 'r')
        except:
            logging.error(f'Settings file not accessible {e}')
            f.close()
            return False
        rs = json.load(f)
        f.close()
        return rs

'''
writes new settings into settings.json
'''
def write_settings(new_settings):
    try:
        f = open(settings, 'w+')
        json.dump(new_settings, f)
    except:
        logging.error(f'Settings file not accessible {e}')
        f.close()
        return False
    finally:
        f.close()
    
        