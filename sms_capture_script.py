from flask import Flask, request, Response, json as fljs 
import os, json, sqlite3 as sql, logging
from sms_send import send_sms
from settings import settings, init_settings, read_settings

app = Flask("Cashpot Onboarder")
db = r'code_cur.db' #Ensure non repeating agents/codes
logging.basicConfig(filename='cashpot_logs.log', level=logging.DEBUG)

'''
Increments strings
'''
def string_incrementer(strn):
    carry = False
    strn = list(strn)
    ln = len(strn)
    for x in range(1, ln+1):
        if strn[ln-x] == 'Z':
            if carry and x == ln:
                strn.append('A')
            carry = True
            strn[ln-x] = 'A'
        else:
            carry = False
            strn[ln-x] = chr(ord(strn[ln-x]) + 1)
            break
    sp = ""
    strn = sp.join(strn)
    return strn

'''
Checks if theres an SQLite instance
Creates one if there isn't, returns
the db connection.
'''
def db_checker():
    if os.path.isfile(db):
        try:
            # does most of the work, gens db if it doesnt exist
            # opens the db file if in rwc 
            conn = sql.connect(db)
        except sql.Error:
            err = f'Already existing file error {e}'
            logging.error(err)
            return False
        return conn
    else:
        try:
            # does most of the work, gens db if it doesnt exist
            # opens the db file if in rwc 
            conn = sql.connect(db)
        except sql.Error:
            err = f"DB creation error {e}"
            logging.error(err)
            return False
        cur = conn.cursor()
        cur.execute('''CREATE TABLE agents
               ( agent_number TEXT NOT NULL, 
                 agent_code TEXT NOT NULL, 
                 dt_text TEXT NOT NULL)''')
        return conn

'''
Check if number already in DB
'''
def check_nmb(cr, nmb):
    cr.execute('''
    SELECT agent_number, agent_code FROM agents
    WHERE agent_number = ?
    ''', [nmb])
    rs = cr.fetchone()
    if rs is None:
        return [False, None]
    else:
        return [True, rs[1]]

'''
Generates the (n) letter code
Packaged together with a sqlite db 
to ensure non-repeating user codes.
SQLite is preferable here for its
serveless lightweight nature.
'''
def gen_code():
    con = db_checker()
    if con is not False:
        cur = con.cursor()
        nre = cur.execute('''  
        SELECT * 
        FROM    agents
        WHERE   rowid = (SELECT MAX(rowid)  FROM agents);
        ''')
        code = cur.fetchone()
        if code is None:
            return "AAA"
        else:
            code = code[1]
        con.close()
        return string_incrementer(code)

'''
Accepts POST form to update
settings
'''
@app.route('/settings/update', methods=['POST'])
def update_settings():
    if request.method == 'POST':
        if os.path.isfile(settings):
            try:
                f = open(settings)
                json.dump(json.load(request.form), f)
            except:
                logging.error(f'Settings didnt update: {e}')
                return Response(status=500)
        else:
            return init_settings()
        return fljs.jsonify({
            "success":"The settings were updated",
        })    

'''
Processes callbacks when an sms 
is sent to the specified shortcode
'''
@app.route('/collector', methods=['GET', 'POST'])
def processor():
    if request.method == 'POST':
        unm = request.form.get("from")
        dt = request.form.get("date")
        text = request.form.get("text")
        snd = send_sms()
        st = read_settings()
        if unm is None or dt is None or text is None:
            logging.error(" The requests form was improperly formated or incomplete, abborting processing...")
            return Response(response=json.dumps({"failed": "invalid form POSTed","required fields": "from, date, text"}), status=400, mimetype='application/json')
        text = text.upper()
        if text == "AGENT":
            cd = gen_code()
            cn = db_checker()
            cr = cn.cursor()
            exists = check_nmb(cr, unm)
            if not exists[0]:
                cr.execute('''
                INSERT INTO agents (agent_number, agent_code, dt_text)
                VALUES( ?,	?, ?);
                ''', [unm, cd, dt])
                cn.commit()
                txt = "{0}{1}{2}".format(st['successful'], ' ', cd)
                snd.sending(unm, txt)
                logging.info("New agent code: {0}, query result: {1}, user: {2}".format(cd, cr.lastrowid, unm))
                return Response(status=200)
            else:
                txt = "{0}{1}{2}".format(st['duplicate'], ' ', exists[1])
                snd.sending(unm, txt)
                logging.info("Duplicate request for code: {0}, query result: {1}, user: {2}".format(exists[1], cr.lastrowid, unm))
                return Response(status=200)

    else:
        return 'Hello, World!'

if __name__ == "__main__":
    app.run('slashdotlabsprojects.com', port=8080)