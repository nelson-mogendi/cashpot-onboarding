import africastalking, logging, json
from settings import settings, read_settings

st = read_settings()

africastalking.initialize(
    username=st['username'],
    api_key=st['sms_api_key']
)

sms = africastalking.SMS

class send_sms():

    def sending(self, unm, msg):
            recipients = [unm]
            message = msg
            sender = st["short-code"]
            try:
                response = sms.send(message, recipients, sender)
                logging.info("Message sent" + json.dumps(response))
            except Exception as e:
                logging.error(f'Send message failed: {e}')